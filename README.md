#ISTS Pattern Classification Algorithm#
ISTS is a time-series classification algorithm that is trained to recognize 25 fundamental behavior modes from dynamic feedback models, such as System Dynamics simulation models. The algorithm is designed by Korhan Kanar (Bogazici University) and it is based on a hidden-markov model.

##Installing##
Windows
Unzip the ISTS-1.0.zip file
From terminal window reach to the ISTS-1.0/ directory where setup.py is located using 'cd' command.
Then type the following command to start the installation.
	python setup.py install
Installation process will locate the library in your current python directory

MAC
Unzip the ISTS-1.0.tar.gz file
From terminal window reach to the ISTS-1.0/ directory where setup.py is located using 'cd' command.
Then type the following command to start the installation.
	python setup.py install
Installation process will locate the library in your current python directory

NOTE : All codes are written in Python 2.7. Therefore, you need Python 2.7 version on your 
computer for installation. If you have a later version of Python on your computer, you can 
switch between versions of Python by programs like Anaconda.

##Documentation(Short)##
First it is required to impot ists library
->from ists import ists

In order to use the main features of the ISTS Library, it is necessary to create an instance from the ISTS class
ex. ists_instance = ists.ists()

After creating an instance of ists class, following functions can be used.
-> testDataClass(y,hypothesisCode) function can be used to check whether given data 'y' is from hypothesized class 'hypothesisCode'.
	Returns hypothesisResult(boolean), table(likelihood table for all classes), weak(0,1)(whether best match is still weak or not) 
ex. hypothesisResult, table, weak = ists_instance.TestDataClass(y,'gr1da')

-> classLikelihoodAll(y) returns likelihood table containing scores of all possible classes.
ex. table = ists_instance.classLikelihoodAll(y)

-> getClassId(y) returns the id of the class that given data is belong to after behavior classification
ex. class_id = ists_instance.getClassId(y)

-> getPeriod(y) returns the period of the given data.
ex. period = ists_instance.getPeriod(y)

-> smooth(y) applies exponential smoothing to the given data and returns the smoothed data.
ex. smoothed_y = ists_instance.smooth(y)

-> normalize(y) normalizes the given data and returns the normalized data
ex. normalized_y = ists_instance.normalize(y)

-> getClassIdByName(name) returns the id of the class used in the classification
ex. class_id = ists_instance.GetClassIdByName('gr1da')

-> getClassNameById(id) returns the name of the class used in the classification
ex. class_name = ists_instance.GetClassNameById(2)

**Functions that can be used without creating an instance

-> autocorrelation(y) returns the autocorrelation of the data  
ex. autocor_y = ists.autocorrelation(y)
-> covariance(y) returns the covariance of the data
ex. cov_y = ists.covariance(y)